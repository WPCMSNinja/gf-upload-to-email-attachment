=== GF Upload to Email Attachment ===
Contributors: billiardgreg
Donate link: http://wpcms.ninja/
Tags: 
Requires at least: 4.2.2
Tested up to: 4.5.3
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This allows you to create a notification in gravity forms of an email that would send with the files being uploaded by that form as an attachment. Checkboxes in the notification area allow for you to setup a notification to attach files

== Description ==

Gravity Forms was built to be able to store all uploaded files to the server and email you a link.  There are times that you need to have that file get attached to the notification email.  You can now tick a checkbox in the notifications area to specify whether or not you want the file attached. If multiple files you are then able to have it attempt to zip before sending too.

Works with both single and multiple upload boxes as well as multiiple notifiations.  As this notification name isn't really used in any other place I thought it would be the easiest way to add this functionality.  If multiple files are attached it attempts to create a zip file to send and after confirmation message is sent it removes the zip file.  Adding NZ so the end of the notification reads GFUEANZ tells the plugin to not zip up the files when attaching.

Utilizes code example from Gravity Forms gforms_notification page modified to attach the files getting uploaded to the notification email based upon last 5 characters of the notification name.    

== Frequently Asked Questions ==

= Where can I get answers to questions? =

You can email greg@wpcms.ninja to receive answers or go to http://wpcms.ninja

== Installation ==

Install plugin and activate.

Check off box in notification settings to enable file attachment.

== Screenshots ==

1. No Screenshot

== Changelog ==

= 1.3.1 =
* Added customization through checkboxes instead of through name modification.
* Kept name modification active to keep old implementation working.

= 1.3 =
* Updated readme.

= 1.2 =
* Added no zip addition to multifile upload.

= 1.1 =
* Added zip functionality to multifile upload.

= 1.0 =
* Updated description and changed to stable version 1.0

= .1 =
* Initial Release of Plugin

== Upgrade Notice ==

= 1.3.1 =
* Modified system to work with checkboxes in notification area instead of modifying the name of the notification.  Made it so plugin still works with name modification.

= 1.3 =
* Updated readme.

= 1.2 =
* Added no zip addition to multifile upload.

= 1.1 =
* Added zip functionality to multifile upload.

= .1 =
* Initial Release of Plugin